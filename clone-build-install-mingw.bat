@echo off

set REPO_NAME=%~n1

call %1

if "%REPO_URL%"=="" goto :norepourl

for %%i in (gcc.exe) do set GCC=%%~$PATH:i
if not exist "%GCC%" set PATH=C:\Qt\Tools\mingw492_32\bin;%PATH%

for %%i in (gcc.exe) do set GCC=%%~$PATH:i
if not exist "%GCC%" goto :nomingw

if not exist src (
    mkdir src
)

if not exist build (
    mkdir build
)

if not exist install (
    mkdir install
)

set REPO_SRC_PATH="src/%REPO_NAME%"

if exist %REPO_SRC_PATH% (
    if exist %REPO_SRC_PATH%/CMakeLists.txt (
        echo Repository already exists, pulling from remote.
        git fetch --recurse-submodules %REPO_SRC_PATH%
    ) else (
        echo Folder already exists but does not seem to be a cloned repository. Stopping now.
        goto :exit
    )
) else (
    git clone --recurse-submodules %REPO_URL% %REPO_SRC_PATH%
)

set REPO_BUILD_PATH="build/%REPO_NAME%"
if not exist %REPO_BUILD_PATH% (
    mkdir %REPO_BUILD_PATH%
)

cd %REPO_BUILD_PATH%
cmake -G "MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=../../install ../../%REPO_SRC_PATH%
mingw32-make
mingw32-make install
cd ../../

goto :exit

:nomingw
echo No MinGW found
goto :exit

:norepourl
echo No Repository url given. Make sure REPO_URL is set before call me.
goto :exit

:exit

set interactive=1
echo %cmdcmdline% | find /i "%~0" >nul
if not errorlevel 1 pause
