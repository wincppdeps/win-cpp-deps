@echo off

:getprojectname

echo.
echo What will your project be called?
echo.
set /p PROJECT_NAME=" "

if exist "%PROJECT_NAME%/" (
    echo.
    echo There is already a project with this name:/ Try a new one.
    goto :getprojectname
)

echo.
echo What framework do you want to use?
echo.
echo  1. GLFW3
echo  2. SDL2
echo  3. SFML
echo  4. FreeGLUT
echo.
set /p M="Select 1, 2, 3, or 4 and press enter: "
 
if %M%==1 (
    echo.
    echo First we clone/build/install GLFW...
    call clone-build-install-mingw.bat glfw.bat
    echo ...done
    echo.
    
    set REPO_URL=https://bitbucket.org/wincppdeps/glfw-project.git    
)
if %M%==2 (
    echo.
    echo First we clone/build/install SDL2...
    call clone-build-install-mingw.bat sdl2.bat
    echo ...done
    echo.
    
    set REPO_URL=https://bitbucket.org/wincppdeps/sdl2-project.git
)
if %M%==3 (
    echo.
    echo First we clone/build/install SFML...
    call clone-build-install-mingw.bat sfml.bat
    echo ...done
    echo.
    
    set REPO_URL=https://bitbucket.org/wincppdeps/sfml-project.git
)
if %M%==4 (
    echo.
    echo First we clone/build/install FreeGLUT...
    call clone-build-install-mingw.bat freeglut.bat
    echo ...done
    echo.
    
    set REPO_URL=https://bitbucket.org/wincppdeps/freeglut-project.git
)

echo.
echo Now we grab the template project from bitbucket.org...
git clone %REPO_URL%     tmp
if errorlevel 1 goto :endonerror
echo ...done

if exist tmp (
    echo.
    echo And finally we create the workspace from the downloaded code...
    cd tmp
    git checkout-index -a -f --prefix="../%PROJECT_NAME%-workspace/%PROJECT_NAME%/"
    cd ../
    rmdir /s /q tmp
            
    if not exist "%cd%/%PROJECT_NAME%-workspace/build" (
        mkdir "%cd%/%PROJECT_NAME%-workspace/build"
    )
    echo ...done!

    start "" "%cd%/%PROJECT_NAME%-workspace"
)

goto :exit

:endonerror
echo oh my gosh, an error...

:exit